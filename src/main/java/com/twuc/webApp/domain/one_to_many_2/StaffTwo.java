package com.twuc.webApp.domain.one_to_many_2;

import javax.persistence.*;

@Entity
public class StaffTwo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;

    public StaffTwo(String name) {
        this.name = name;
    }

    public StaffTwo() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
