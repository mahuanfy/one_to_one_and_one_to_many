package com.twuc.webApp.domain.one_to_many_2;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffTwoRepository extends JpaRepository<StaffTwo, Long> {
}
