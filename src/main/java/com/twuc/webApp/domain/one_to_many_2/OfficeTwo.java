package com.twuc.webApp.domain.one_to_many_2;

import javax.persistence.*;

@Entity
public class OfficeTwo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;

    public OfficeTwo(String name) {
        this.name = name;
    }

    public OfficeTwo() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
