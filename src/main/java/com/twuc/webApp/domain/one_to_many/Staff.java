package com.twuc.webApp.domain.one_to_many;

import javax.persistence.*;

@Entity
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;

    public Staff(String name) {
        this.name = name;
    }

    public Staff() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
