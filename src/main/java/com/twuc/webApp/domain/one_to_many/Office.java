package com.twuc.webApp.domain.one_to_many;

import javax.persistence.*;

@Entity
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;

    public Office(String name) {
        this.name = name;
    }

    public Office() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
