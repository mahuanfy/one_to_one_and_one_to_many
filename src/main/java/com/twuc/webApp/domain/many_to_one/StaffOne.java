package com.twuc.webApp.domain.many_to_one;

import javax.persistence.*;

@Entity
public class StaffOne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;

    public StaffOne(String name) {
        this.name = name;
    }

    public StaffOne() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
