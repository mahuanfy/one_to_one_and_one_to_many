package com.twuc.webApp.domain.many_to_one;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeOneRepository extends JpaRepository<OfficeOne, Long> {
}
