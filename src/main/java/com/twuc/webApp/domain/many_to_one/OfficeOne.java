package com.twuc.webApp.domain.many_to_one;

import javax.persistence.*;

@Entity
public class OfficeOne {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;

    public OfficeOne(String name) {
        this.name = name;
    }

    public OfficeOne() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
