package com.twuc.webApp.domain.one_to_many;

import com.twuc.webApp.domain.JpaTestBase;
import com.twuc.webApp.domain.one_to_one.Profile;
import com.twuc.webApp.domain.one_to_one.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

class OneToManyTest extends JpaTestBase {

    @Autowired
    private OfficeRepository officeRepository;
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private EntityManager entityManager;

    void flushAndClear(Runnable run) {
        run.run();
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    void should_() {
        flushAndClear(() -> {
            Office office = new Office("xi`an");
            Staff staff = new Staff("O_o");
            staffRepository.save(staff);
        });
        flushAndClear(()->{
        });
    }
}
